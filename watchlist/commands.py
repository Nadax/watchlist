import click

from watchlist import app, db
from watchlist.models import User, Movie


@app.cli.command()
@click.option('--drop', is_flag=True, help='创建之前销毁数据库')
def initdb(drop):
  """初始化数据库"""
  if (drop):
    db.drop_all()
  db.create_all()
  click.echo('完成!')


@app.cli.command()
def forge():
  """生成假数据"""
  db.create_all()
  name = 'nada'
  movies = [
    {'title': 'My Neighbor Totoro', 'year': '1988'},
    {'title': 'Dead Poets Society', 'year': '1989'},
    {'title': 'A Perfect World', 'year': '1993'},
    {'title': 'Leon', 'year': '1994'},
    {'title': 'Mahjong', 'year': '1996'},
    {'title': 'Swallowtail Butterfly', 'year': '1996'},
    {'title': 'King of Comedy', 'year': '1999'},
    {'title': 'Devils on the Doorstep', 'year': '1999'},
    {'title': 'WALL-E', 'year': '2008'},
    {'title': 'The Pork of Music', 'year': '2012'},
  ]
  user = User(name=name)
  db.session.add(user)
  for movie in movies:
    movie = Movie(title=movie['title'], year=movie['year'])
    db.session.add(movie)
  db.session.commit()
  click.echo('完成!')


@app.cli.command()
@click.option('--username', prompt=True, help='用户账号')
@click.option('--password', prompt=True, hide_input=True, confirmation_prompt=True, help='用户密码')
def admin(username, password):
  """创建用户"""
  db.create_all()
  user = User.query.first()
  if user is not None:
    click.echo('更新用户...')
    user.username = username
    user.set_password(password)
  else:
    click.echo('创建用户...')
    user = User(username=username, name='Admin')
    user.set_password(password)
    db.session.add(user)
  db.session.commit()
  click.echo('完成!')
