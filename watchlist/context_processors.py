from watchlist import app

from watchlist.models import User

@app.context_processor
def inject_user():
  user = User.query.first()
  return {'user': user}
