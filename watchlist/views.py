from flask import render_template, url_for, request, redirect, flash
from flask_login import login_user, logout_user, login_required, current_user

from watchlist import app, db
from watchlist.models import User, Movie


@app.route('/', methods=['GET', 'POST'])
def index():
  if request.method == 'POST':
    if not current_user.is_authenticated:
      return redirect(url_for('index'))
    title = request.form.get('title')
    year = request.form.get('year')
    if not title or not year or len(year) > 4 or len(title) > 60:
      flash('不合法的参数')
      return redirect(url_for('index'))
    movie = Movie(title=title, year=year)
    db.session.add(movie)
    db.session.commit()
    flash('创建成功')
    return redirect(url_for('index'))
  else:
    movies = Movie.query.all()
    return render_template('index.html', movies=movies)


@app.route('/movie/edit/<int:movie_id>', methods=['GET', 'POST'])
@login_required
def edit(movie_id):
  movie = Movie.query.get_or_404(movie_id)
  if request.method == 'POST':
    title = request.form.get('title')
    year = request.form.get('year')
    if not title or not year or len(year) > 4 or len(title) > 60:
      flash('不合法的参数')
      return redirect(url_for('edit', id=id))
    movie.title = title
    movie.year = year
    db.session.commit()
    flash('更新成功')
    return redirect(url_for('index'))
  return render_template('edit.html', movie=movie)


@app.route('/movie/delete/<int:movie_id>', methods=['POST'])
@login_required
def delete(movie_id):
  movie = Movie.query.get_or_404(movie_id)
  db.session.delete(movie)
  db.session.commit()
  flash('删除成功')
  return redirect(url_for('index'))


@app.route('/settings', methods=['GET', 'POST'])
@login_required
def settings():
  if request.method == 'POST':
    name = request.form.get('name')
    if not name or len(name) > 20:
      flash('不合法的参数')
      return redirect(url_for('settings'))
    current_user.name = name
    db.session.commit()
    flash('更新成功')
    return redirect(url_for('index'))
  return render_template('settings.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
  if request.method == 'POST':
    username = request.form.get('username')
    password = request.form.get('password')
    if not username or not password:
      flash('不合法的参数')
      return redirect(url_for('login'))
    user = User.query.first()
    if username == user.username and user.validate_password(password):
      login_user(user)
      flash('登录成功')
      return redirect(url_for('index'))
    flash('错误的 username 或 password')
    return redirect(url_for('login'))
  return render_template('login.html')


@app.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
  logout_user()
  flash('Goodbye~')
  return redirect(url_for('index'))
